/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caroserver;

/**
 *
 * @author Admin
 */
import caroserver.model.User;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Admin
 */
/*
Node: Kiểu dữ liệu cơ bản của DOM.
Element: Phần lớn các đối tượng bạn sẽ xử lý là Elements.
Attr: Biểu diễn một thuộc tính của một phần tử.
Text: Nội dung thực tế của Element hoặc Attr.
Document: Biểu diễn toàn bộ tài liệu XML. Một đối tượng Document thường được gọi là cây DOM.

Document.getDocumentElement(): Trả về phần tử gốc của tài liệu.
Node.getFirstChild(): Trả về con đầu tiên của một Node đã cho.
Node.getLastChild(): Trả về con cuối của một Node đã cho.
Node.getNextSibling(): Phương thức này trả về sibling (anh em) tiếp theo của một Node đã cho.
Node.getPreviousSibling(): Phương thức này trả về sibling trước của một Node đã cho.
Node.getAttribute(attrName): Đối với một Node đã cho, nó trả về thuộc tính với tên được yêu cầu attrName.

//trả về thuộc tính cụ thể
getAttribute("attributeName");
 
//trả về một Map (table) của các cặp name/value
getAttributes();

//trả về một list của phần tử con của tên được chỉ định
getElementsByTagName("subelementName");
 
//trả về list tất cả các node con
getChildNodes();
 */
public class DatabaseXML {

    File inputFile = new File("F:\\Study\\LapTrinhMang\\CaroServer_1\\src\\caroserver\\model\\caro.xml");
    DocumentBuilderFactory dbFactory;
    DocumentBuilder dBuilder;
    Document doc;
    TransformerFactory transformerFactory;
    Transformer transformer;

    public DatabaseXML() {
        try {
            dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(inputFile);
            transformerFactory = TransformerFactory.newInstance();
            transformer = transformerFactory.newTransformer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<User> readListUsers() {
        List<User> listUsers = new ArrayList<>();
        User user = null;
        try {
            doc.getDocumentElement().normalize();
            // in phần tử gốc ra màn hình
            //System.out.println("Phần tử gốc: " + doc.getDocumentElement().getNodeName());

            // đọc tất cả các phần tử có tên thẻ là "user"
            NodeList nodeListUser = doc.getElementsByTagName("user");

            // duyệt các phần tử user
            for (int i = 0; i < nodeListUser.getLength(); i++) {
                // tạo đối tượng user
                user = new User();
                // đọc các thuộc tính của user
                Node nNode = nodeListUser.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    user.setId(Integer.parseInt(eElement.getAttribute("id")));
                    user.setUsername(eElement.getElementsByTagName("username").item(0).getTextContent());
                    user.setPassword(eElement.getElementsByTagName("password").item(0).getTextContent());
                }
                // add đối tượng user vào listUser
                listUsers.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listUsers;
    }

    public void updtaeXML(User user) {
        NodeList nodeListUser = doc.getElementsByTagName("user");

        for (int i = 0; i < nodeListUser.getLength(); i++) {
            Node node = nodeListUser.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                if (user.getId() == Integer.parseInt(element.getAttribute("id"))) {
                    element.getElementsByTagName("username").item(0).setTextContent(user.getUsername());
                    element.getElementsByTagName("password").item(0).setTextContent(user.getPassword());
                }
            }
        }
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(inputFile);
        try {
            transformer.transform(source, result);
        } catch (TransformerException ex) {
            ex.printStackTrace();
        }
    }

    public boolean saveXML(User user) {
        boolean kq = true;
        List<User> listUsers = readListUsers();
        for (int i = 0; i < listUsers.size(); i++) {
            if (listUsers.get(i).getUsername().equals(user.getUsername())) {
                kq = false;
            }
        }
        if (kq) {
            user.setId(listUsers.get(listUsers.size()-1).getId() + 1);
            Element rootElement = doc.getDocumentElement();
            // tạo phần tử user
            Element user1 = doc.createElement("user");
            rootElement.appendChild(user1);
            // tạo thuộc tính id cho user1
            Attr attr1 = doc.createAttribute("id");
            attr1.setValue(user.getId() + "");
            user1.setAttributeNode(attr1);
            // tạo thẻ username
            Element username = doc.createElement("username");
            username.appendChild(doc.createTextNode(user.getUsername()));
            user1.appendChild(username);
            // tạo thẻ password
            Element password = doc.createElement("password");
            password.appendChild(doc.createTextNode(user.getPassword()));
            user1.appendChild(password);

            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(inputFile);
            try {
                transformer.transform(source, result);
            } catch (TransformerException ex) {
                ex.printStackTrace();
            }
        }
        return kq;
    }
    
    public boolean checkLogin(String username, String password){
        boolean check = false;
        List<User> listUsers = readListUsers();
        for(int i = 0; i < listUsers.size(); i++){
            if(listUsers.get(i).getUsername().equals(username) && listUsers.get(i).getPassword().equals(password)) check = true;
        }
        return check;
    }
}
