package caroserver;

import java.sql.*;

public class Database {

    Connection con;
    PreparedStatement ps;
    ResultSet rs;

    private static final String DB_NAME = "CaroGame";
    private static final String DB_USERNAME = "root";
    private static final String DB_PASSWORD = "123456";

    Database() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + DB_NAME, DB_USERNAME, DB_PASSWORD);
            ps = con.prepareStatement("SELECT * FROM user WHERE username = ? AND password = ?");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean checkLogin(String username, String password) {
        try {
            //Thay vào tham số vao "?" trong query 
            ps.setString(1, username);
            ps.setString(2, password);
            //Thực thi prepared statement
            rs = ps.executeQuery();

            //Kiểm tra tính tồn tại của tài khoản
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println("error while validating" + e);
            e.printStackTrace();
            return false;
        }
    }
}
