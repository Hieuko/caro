package caroserver;

import caroserver.model.User;
import java.io.*;
import java.net.*;

public class ThreadSocket extends Thread {

    Socket socket;
    int port;
    DatabaseXML db;
    Address add;

    ThreadSocket(Socket socket, int port) {
        this.socket = socket;
        this.port = port;
        this.db = new DatabaseXML();
    }

    @Override
    public void run() {
        try {
            System.out.println("------------" + port);
            //Tạo luồng nhận dữ liệu từ bàn phím
            DataInputStream inFromServer = new DataInputStream(System.in);
            //Tạo luồng nhận dữ liệu từ Client
            DataInputStream inFromClient = new DataInputStream(socket.getInputStream());
            //Tạo luồng gửi dữ liệu về Client
            DataOutputStream outToClient = new DataOutputStream(socket.getOutputStream());

            while (true) {
                String string = inFromClient.readLine();
                System.out.println(string);
                String[] ss = string.split("-");

                //KIỂM TRA ĐĂNG NHẬP
                if (ss[0].equals("1")) {

                    System.out.println("Username,Pwd:" + ss[1] + "," + ss[2]);
                    if (db.checkLogin(ss[1], ss[2])) {
                        outToClient.writeBytes("1\n");

                    } else {
                        outToClient.writeBytes("0\n");
                    }
                }

                /**
                 * ĐĂNG KÝ TÀI KHOẢN
                 */
                if (ss[0].equals("2")) {
                    User user = new User();
                    user.setUsername(ss[1]);
                    user.setPassword(ss[2]);
                    if(db.saveXML(user))
                         outToClient.writeBytes("1\n");
                }

                /**
                 * TẠO PHÒNG
                 */
                if (ss[0].equals("3")) {
                    System.out.println("Một người chơi ở IP: '" + ss[2] + "' vừa tạo phòng.");
                    add.port.add(port);
                    add.ip.add(ss[2]);
                    add.name.add(ss[1]);
                    String _string = ss[2] + "-" + port;
                    System.out.println("IP tao Server gui ve Client: " + _string);
                    //gui Address ve client vừa tạo phòng
                    outToClient.writeBytes(_string + "\n");
                }

                /**
                 * THAM GIA PHÒNG
                 */
                if (ss[0].equals("4")) {
                    String _strPort = "";
                    String _strIp = "";
                    String _strName = "";
                    for (int i = 0; i < add.port.size(); i++) {
                        _strPort = _strPort + add.port.get(i) + "-";
                        _strIp = _strIp + add.ip.get(i) + "-";
                        _strName = _strName + add.name.get(i) + "-";
                    }

                    outToClient.writeBytes(_strPort + "\n");
                    outToClient.writeBytes(_strIp + "\n");
                    outToClient.writeBytes(_strName + "\n");

                    System.out.println(_strPort);
                    System.out.println(_strIp);
                    System.out.println(_strName);

                    String[] sss = _strName.split("-");
                    System.out.println(sss.length);
                    for (int i = 0; i < sss.length; i++) {
                        System.out.println(sss[i]);
                    }
                }

                if (ss[0].equals("5")) {
                    int j = 0;
                    for (j = 0; j < add.name.size(); j++) {
                        if (ss[1].equals(add.name.get(j))) {
                            break;
                        }
                    }
                    add.port.remove(j);
                    add.ip.remove(j);
                    add.name.remove(j);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
