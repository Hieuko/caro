package caroclient;

import java.util.Vector;

public class Address {

    public static Vector<Integer> port = new Vector<Integer>();
    public static Vector<String> ip = new Vector<String>();
    public static Vector<String> name = new Vector<String>();

    public Address(Integer port, String ip) {
        this.port.add(port);
        this.ip.add(ip);
    }

}
